extension StringExtension on String {
  String capitalizeFirst() {
    return trim().isNotEmpty
        ? "${this[0].toUpperCase()}${substring(1).toLowerCase()}"
        : "";
  }

  String capitalizeEachWord() {
    String a = this;
    return a.split(' ').map((word) {
      // return "${word[0].toUpperCase()}${substring(1).toLowerCase()}";
      return word.capitalizeFirst();
    }).join(' ');
  }
}
