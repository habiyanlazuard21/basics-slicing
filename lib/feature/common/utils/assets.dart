class AppIcon {
  static const _baseIconAssets = './assets/icons';

  static const logo = '$_baseIconAssets/logo.png';
  static const logoMain = '$_baseIconAssets/logo_main.png';
  static const filter = '$_baseIconAssets/ic_filter.png';
}

class AppIllustrations {
  static const _baseIllustrationAssets = './assets/illustrations';
  static const dummy = '$_baseIllustrationAssets/ill_dummy_image.png';
}

class AppImages {
  static const _baseImagesAssets = './assets/images';

  static const intro = '$_baseImagesAssets/img_intro_sign_in_up.png';
  static const itemRecentImage = '$_baseImagesAssets/image_dumy.png';
  static const basketball = '$_baseImagesAssets/basketball.png';
  static const town1 = '$_baseImagesAssets/Town 1.png';
}
