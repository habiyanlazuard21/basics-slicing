import 'package:flutter/material.dart';

import '../../../feature/feature.dart';

class PageRouter {
  Route<dynamic>? getRoute(
    RouteSettings settings,
  ) {
    switch (settings.name) {
      /* Intro */
      //-------------------------------------------------------
      case PagePath.intro:
        return _buildRouter(
          settings: settings,
          builder: (args) => const IntroPage(),
        );
      //------------------------------------------------------

      /* Login */
      //-------------------------------------------------------
      case PagePath.login:
        return _buildRouter(
          settings: settings,
          builder: (args) => const LoginPage(),
        );
      //------------------------------------------------------

      /* Sign Up */
      //-------------------------------------------------------
      case PagePath.signUp:
        return _buildRouter(
          settings: settings,
          builder: (args) => const SignUpPage(),
        );
      //------------------------------------------------------

      /* Bottom Navigation */
      //-------------------------------------------------------
      case PagePath.bottomNavBar:
        return _buildRouter(
          settings: settings,
          builder: (args) => BottomNavigation(selectedIndex: args as int? ?? 0),
        );
      //------------------------------------------------------

      /* Home */
      //-------------------------------------------------------
      case PagePath.home:
        return _buildRouter(
          settings: settings,
          builder: (args) => HomeScreen(
            params: (args as HomeScreenParams?) ?? HomeScreenParams(),
          ),
        );
      //------------------------------------------------------

      /* Detail Event */
      //-------------------------------------------------------
      case PagePath.detailEventPage:
        return _buildRouter(
          settings: settings,
          builder: (args) => DetailEventPage(
            params: args as DetailEventParams? ?? DetailEventParams(),
          ),
        );
      //------------------------------------------------------

      /* Detail Image Event */
      //-------------------------------------------------------
      case PagePath.detailImageEventPage:
        return _buildRouter(
          settings: settings,
          builder: (args) => DetailImageEventPage(
            params: args as DetailImageEventParams? ?? DetailImageEventParams(),
          ),
        );
      //------------------------------------------------------

      default:
        return null;
    }
  }

  Route<dynamic> _buildRouter({
    required RouteSettings settings,
    required Widget Function(Object? arguments) builder,
  }) {
    return MaterialPageRoute(
      settings: settings,
      builder: (_) => builder(settings.arguments),
    );
  }
}
