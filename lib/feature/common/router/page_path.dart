class PagePath {
  /* Home */
  static const intro = '/';
  static const bottomNavBar = '/bottom_nav_bar';
  static const home = '/home';
  static const login = '/login';
  static const signUp = '/sign_up';

  /* verification OTP */
  static const verificationOtp = '/verification_otp_page';

  /* Event */
  static const detailEventPage = '/detail_event_page';
  static const detailImageEventPage = '/detail_image_event_page';
}
