import 'package:flutter/material.dart';

import '../../../../lib.dart';

class ButtonPrimary extends StatelessWidget {
  const ButtonPrimary(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.main,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
    TextDecoration? textDecoration,
    Color foregroundbutton = Colors.transparent,
    double? elevation,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        _textDecoration = textDecoration,
        _foregroundButton = foregroundbutton,
        _elevation = elevation,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;
  final TextDecoration? _textDecoration;
  final Color? _foregroundButton;
  final double? _elevation;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: _elevation,
          backgroundColor: _buttonColor,
          foregroundColor: _foregroundButton,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Text(
          _label,
          style: TextStyle(
            fontSize: _fontSize,
            color: _labelColor,
            fontWeight: _fontWeight,
            decoration: _textDecoration,
          ),
          textAlign: _textAlign,
          overflow: TextOverflow.ellipsis,
        ),
      ),
    );
  }
}

class ButtonFilter extends StatelessWidget {
  const ButtonFilter({
    Key? key,
    required Function() onPressed,
    Color buttonColor = AppColors.white,
    Color borderColor = AppColors.black,
    double borderWidth = 2,
    double borderRadius = AppBorderRadius.normal,
    double height = AppButtonSize.small,
    double? width,
    double elevation = 0,
  })  : _onPressed = onPressed,
        _buttonColor = buttonColor,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _height = height,
        _width = width,
        _elevation = elevation,
        super(key: key);

  final Function() _onPressed;
  final Color _buttonColor;
  final Color _borderColor;
  final double _borderWidth;
  final double _borderRadius;
  final double? _height;
  final double? _width;
  final double? _elevation;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height,
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          elevation: _elevation,
          backgroundColor: _buttonColor,
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _onPressed,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Image.asset(AppIcon.filter, height: AppIconSize.tiny),
            const Gap(width: AppGap.normal / 2),
            Text(
              "Filter",
              style: AppTextStyle.regular
                  .copyWith(fontSize: AppFontSize.extraSmall),
            ),
          ],
        ),
      ),
    );
  }
}

class ButtonPrimaryContinue extends StatelessWidget {
  const ButtonPrimaryContinue(
    String label, {
    Key? key,
    required Function() onPressed,
    TextAlign textAlign = TextAlign.center,
    bool isButtonDisabled = false,
    FontWeight fontWeight = AppFontWeight.bold,
    Color buttonColor = AppColors.main,
    Color labelColor = AppColors.white,
    Color borderColor = Colors.transparent,
    double borderWidth = 1,
    double borderRadius = AppBorderRadius.tiny,
    double fontSize = AppFontSize.normal,
    double paddingVertical = 0,
    double paddingHorizontal = AppGap.normal,
    double? height,
    double? width,
  })  : _label = label,
        _onPressed = onPressed,
        _textAlign = textAlign,
        _fontWeight = fontWeight,
        _buttonColor = buttonColor,
        _labelColor = labelColor,
        _isButtonDisabled = isButtonDisabled,
        _borderColor = borderColor,
        _borderWidth = borderWidth,
        _borderRadius = borderRadius,
        _fontSize = fontSize,
        _paddingVertical = paddingVertical,
        _paddingHorizontal = paddingHorizontal,
        _height = height,
        _width = width,
        super(key: key);

  final String _label;
  final Function() _onPressed;
  final TextAlign _textAlign;
  final FontWeight _fontWeight;
  final Color _buttonColor;
  final Color? _labelColor;
  final Color _borderColor;
  final double _borderWidth;
  final bool _isButtonDisabled;
  final double _borderRadius;
  final double _fontSize;
  final double _paddingVertical;
  final double _paddingHorizontal;
  final double? _height;
  final double? _width;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: _width,
      height: _height ??
          ResponsiveUtils(context).getResponsiveSize(
            AppButtonSize.normal,
          ),
      child: ElevatedButton(
        style: ElevatedButton.styleFrom(
          backgroundColor: _buttonColor,
          padding: EdgeInsets.symmetric(
            vertical: _paddingVertical,
            horizontal: _paddingHorizontal,
          ),
          tapTargetSize: MaterialTapTargetSize.shrinkWrap,
          side: BorderSide(color: _borderColor, width: _borderWidth),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(
              _borderRadius,
            ),
          ),
        ),
        onPressed: _isButtonDisabled ? null : _onPressed,
        child: Stack(
          alignment: Alignment.center,
          children: [
            Text(
              _label,
              style: TextStyle(
                fontSize: _fontSize,
                color: _labelColor,
                fontWeight: _fontWeight,
              ),
              textAlign: _textAlign,
              overflow: TextOverflow.ellipsis,
            ),
            Container(
              alignment: Alignment.centerRight,
              padding: const EdgeInsets.only(right: AppGap.normal),
              child: Icon(
                Icons.arrow_forward_ios,
                size: AppIconSize.normal,
                color: _labelColor,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
