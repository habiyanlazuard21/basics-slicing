import 'package:flutter/material.dart';

import '../../../../lib.dart';

class CustomInkwell extends StatelessWidget {
  final Function()? _onTap;
  final Widget _child;
  const CustomInkwell({
    Key? key,
    Function()? onTap,
    required Widget child,
  })  : _onTap = onTap,
        _child = child,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: AppColors.white,
      child: InkWell(
        onTap: _onTap,
        child: _child,
      ),
    );
  }
}
