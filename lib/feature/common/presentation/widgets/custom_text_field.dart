import 'package:flutter/material.dart';

import '../../../../lib.dart';

class CustomTextFormFieldSearch extends StatelessWidget {
  final TextEditingController _controller;
  final Function(String)? _onSubmitted;
  final Function()? _onPressed;
  final String? _hint;
  const CustomTextFormFieldSearch({
    Key? key,
    required TextEditingController controller,
    required Function(String)? onSubmitted,
    required Function()? onPressed,
    String? hint,
  })  : _controller = controller,
        _onSubmitted = onSubmitted,
        _onPressed = onPressed,
        _hint = hint,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 42,
      child: TextField(
        controller: _controller,
        decoration: InputDecoration(
            suffixIcon: IconButton(
              onPressed: _onPressed,
              icon: const Icon(Icons.search, color: Colors.black, size: 20),
            ),
            filled: true,
            fillColor: AppColors.white,
            hintText: _hint ?? "Search Something",
            hintStyle: AppTextStyle.regular.copyWith(
              color: Colors.black54,
            ),
            contentPadding: const EdgeInsets.all(12),
            border: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black, width: 1),
              borderRadius: BorderRadius.circular(8),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black, width: 1),
              borderRadius: BorderRadius.circular(8),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black, width: 1),
              borderRadius: BorderRadius.circular(8),
            ),
            disabledBorder: OutlineInputBorder(
              borderSide: const BorderSide(color: Colors.black, width: 1),
              borderRadius: BorderRadius.circular(8),
            )),
        onSubmitted: _onSubmitted,
      ),
    );
  }
}

class CustomTextFormField extends StatefulWidget {
  const CustomTextFormField({
    Key? key,
    required TextFieldEntity textFieldEntity,
    int maxLines = 1,
    Color backgroundDisable = TextFieldColors.backgroundDisable,
    String? Function(String?)? validator,
    Function(String)? onChanged,
    int? maxLength,
    Widget? widgetSuffix,
  })  : _textFieldEntity = textFieldEntity,
        _maxLines = maxLines,
        _backgroundDisable = backgroundDisable,
        _validator = validator,
        _onChanged = onChanged,
        _maxLength = maxLength,
        _widgetSuffix = widgetSuffix,
        super(key: key);

  final TextFieldEntity _textFieldEntity;
  final int _maxLines;
  final Color _backgroundDisable;
  final String? Function(String?)? _validator;
  final Function(String)? _onChanged;
  final int? _maxLength;
  final Widget? _widgetSuffix;

  @override
  State<CustomTextFormField> createState() => _CustomTextFormFieldState();
}

class _CustomTextFormFieldState extends State<CustomTextFormField> {
  bool _isObscureText = false;

  @override
  void initState() {
    _isObscureText = widget._textFieldEntity.isPassword;

    widget._textFieldEntity.focusNode?.addListener(() {
      if (widget._textFieldEntity.focusNode?.hasFocus ?? false) {
      } else {
        widget._textFieldEntity.focusNode?.unfocus();
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: widget._textFieldEntity.label.trim().isNotEmpty,
          child: Text(
            widget._textFieldEntity.label,
            style: AppTextStyle.semiBold,
          ),
        ),
        Visibility(
          visible: widget._textFieldEntity.label.trim().isNotEmpty,
          child: const Gap(),
        ),
        Row(
          children: [
            Expanded(
              child: TextFormField(
                controller: widget._textFieldEntity.textController,
                autovalidateMode: AutovalidateMode.onUserInteraction,
                textInputAction: widget._textFieldEntity.textInputAction,
                enabled: widget._textFieldEntity.isEnabled,
                keyboardType: widget._textFieldEntity.keyboardType,
                inputFormatters: widget._textFieldEntity.inputFormatters,
                maxLines: widget._maxLines,
                onChanged: widget._onChanged,
                decoration: InputDecoration(
                  hintText: widget._textFieldEntity.hint,
                  counterText: "",
                  border: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                  ),
                  enabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                  ),
                  disabledBorder: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                  ),
                  focusedBorder: const OutlineInputBorder(
                    borderSide: BorderSide.none,
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                  ),
                  errorBorder: const OutlineInputBorder(
                    borderSide: BorderSide(color: Colors.red, width: 1.5),
                    borderRadius: BorderRadius.all(
                      Radius.circular(4),
                    ),
                  ),
                  hintStyle: AppTextStyle.regular.copyWith(
                    color: AppColors.textBlack,
                  ),
                  contentPadding: const EdgeInsets.all(12),
                  filled: true,
                  fillColor: widget._textFieldEntity.isEnabled
                      ? const Color.fromRGBO(
                          235,
                          229,
                          222,
                          .5,
                        ) //TextFieldColors.backgroundEnable
                      : widget._backgroundDisable,
                  suffixIcon: Visibility(
                    visible: widget._textFieldEntity.isPassword,
                    child: IconButton(
                      color: AppColors.main,
                      icon: Icon(
                        _isObscureText
                            ? Icons.visibility
                            : Icons.visibility_off,
                      ),
                      onPressed: () {
                        setState(() {
                          _isObscureText = !_isObscureText;
                        });
                      },
                    ),
                  ),
                  errorStyle: TextStyle(
                    color: Theme.of(context).colorScheme.error,
                  ),
                ),
                maxLength: widget._maxLength,
                style:
                    AppTextStyle.regular.copyWith(color: AppColors.textBlack),
                obscureText: _isObscureText,
                validator:
                    widget._validator ?? widget._textFieldEntity.validator,
              ),
            ),
            widget._widgetSuffix ?? const SizedBox(),
          ],
        ),
      ],
    );
  }
}

class CustomTextFormFieldOtp extends StatefulWidget {
  const CustomTextFormFieldOtp({
    Key? key,
    required TextFieldEntity textFieldEntity,
    required Function(String)? onChanged,
  })  : _textFieldEntity = textFieldEntity,
        _onChanged = onChanged,
        super(key: key);

  final TextFieldEntity _textFieldEntity;
  final Function(String)? _onChanged;

  @override
  State<CustomTextFormFieldOtp> createState() => _CustomTextFormFieldOtpState();
}

class _CustomTextFormFieldOtpState extends State<CustomTextFormFieldOtp> {
  @override
  void initState() {
    widget._textFieldEntity.focusNode?.addListener(() {
      if (widget._textFieldEntity.focusNode?.hasFocus ?? false) {
      } else {
        widget._textFieldEntity.focusNode?.unfocus();
      }
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 56,
      child: TextFormField(
        controller: widget._textFieldEntity.textController,
        keyboardType: widget._textFieldEntity.keyboardType,
        inputFormatters: widget._textFieldEntity.inputFormatters,
        style: AppTextStyle.semiBold.copyWith(
          fontSize: AppFontSize.large,
          color: AppColors.main,
        ),
        decoration: InputDecoration(
          counterText: '',
          filled: true,
          fillColor: const Color(0xFFF4F0ED),
          hintText: "",
          contentPadding: const EdgeInsets.all(12.0),
          focusedErrorBorder: _border(),
          enabledBorder: _border(),
          focusedBorder: _border(),
          border: _border(),
          errorBorder: const OutlineInputBorder(
            borderRadius:
                BorderRadius.all(Radius.circular(AppBorderRadius.large / 2)),
            borderSide: BorderSide(color: AppColors.red, width: 2),
          ),
        ),
        textAlign: TextAlign.center,
        maxLength: 1,
        onChanged: widget._onChanged,
        focusNode: widget._textFieldEntity.focusNode,
        validator: widget._textFieldEntity.validator,
      ),
    );
  }

  InputBorder _border() {
    return const OutlineInputBorder(
      borderRadius:
          BorderRadius.all(Radius.circular(AppBorderRadius.large / 2)),
      borderSide: BorderSide(
        color: Color(0xFFF4F0ED),
        width: 2,
      ),
    );
  }
}
