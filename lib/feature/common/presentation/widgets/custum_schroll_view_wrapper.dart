import 'package:flutter/material.dart';

class CustomSingleChildScrollViewWrapper extends StatelessWidget {
  const CustomSingleChildScrollViewWrapper({
    Key? key,
    Function()? onTap,
    required Widget child,
    ScrollPhysics? physics,
  })  : _onTap = onTap,
        _child = child,
        _physics = physics,
        super(key: key);

  final Function()? _onTap;
  final Widget _child;
  final ScrollPhysics? _physics;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        //TODO: need funtion
        FocusScope.of(context).unfocus();
        if (_onTap != null) {
          _onTap;
        }
      },
      splashColor: Colors.transparent,
      child: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          physics: _physics,
          child: _child,
        ),
      ),
    );
  }
}
