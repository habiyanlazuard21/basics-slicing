import 'package:flutter/material.dart';

import '../../../../lib.dart';

class CustomAppBar extends StatelessWidget {
  final String _title;
  final bool _automaticallyImplyLeading;
  final Function()? _onPressedBack;
  final Function()? _onTapShare;
  final double _elevation;
  const CustomAppBar({
    Key? key,
    required String title,
    bool automaticallyImplyLeading = true,
    Function()? onPressedBack,
    Function()? onTapShare,
    double elevation = .5,
  })  : _title = title,
        _automaticallyImplyLeading = automaticallyImplyLeading,
        _onPressedBack = onPressedBack,
        _onTapShare = onTapShare,
        _elevation = elevation,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: Colors.white,
      foregroundColor: Colors.black,
      elevation: _elevation,
      centerTitle: false,
      titleSpacing: 0,
      automaticallyImplyLeading: _automaticallyImplyLeading,
      title: Text(
        _title,
        maxLines: 2,
        style: AppTextStyle.semiBold.copyWith(
          fontSize: AppFontSize.large,
        ),
      ),
      leading: _onPressedBack != null
          ? Builder(
              builder: (BuildContext context) {
                return IconButton(
                  icon: const Icon(Icons.arrow_back),
                  onPressed: _onPressedBack,
                  tooltip: "Back",
                );
              },
            )
          : null,
      actions: [
        Visibility(
          visible: _onTapShare != null,
          child: GestureDetector(
            onTap: _onTapShare,
            child: Container(
              width: 20,
              height: 20,
              margin: const EdgeInsets.only(right: AppGap.medium),
              child: const Icon(Icons.share),
            ),
          ),
        ),
      ],
    );
  }
}
