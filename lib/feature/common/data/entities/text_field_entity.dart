import 'package:currency_text_input_formatter/currency_text_input_formatter.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:form_validator/form_validator.dart';

class TextFieldEntity {
  TextEditingController textController;
  String hint;
  String label;
  bool isEnabled;
  bool isPassword;
  TextInputType keyboardType;
  bool? isAutofocus;
  TextInputAction? textInputAction;
  FocusNode? focusNode;
  String? Function(String?)? validator;
  List<TextInputFormatter>? inputFormatters;

  TextFieldEntity({
    required this.textController,
    required this.hint,
    this.label = "",
    this.isPassword = false,
    this.isEnabled = true,
    this.isAutofocus = false,
    this.keyboardType = TextInputType.text,
    this.textInputAction = TextInputAction.next,
    this.focusNode,
    this.validator,
    this.inputFormatters,
  }) {
    ValidationBuilder.setLocale('id');
  }

  static final List<TextFieldEntity> authLogin = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Email",
      keyboardType: TextInputType.emailAddress,
      focusNode: FocusNode(),
      validator: (value) {
        if (value == null || value.trim().isEmpty) {
          return 'Please enter your email';
        } else if (!EmailValidator.validate(value.trim())) {
          return 'Invalid email format';
        }

        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Password",
      isPassword: true,
      textInputAction: TextInputAction.done,
      focusNode: FocusNode(),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter your password';
        } else if (value.length < 8) {
          return 'Password must be at least 8 characters';
        }

        return null;
      },
    ),
  ];

  static final List<TextFieldEntity> authRegister = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Email",
      keyboardType: TextInputType.emailAddress,
      focusNode: FocusNode(),
      validator: (value) {
        if (value == null || value.trim().isEmpty) {
          return 'Please enter your email';
        } else if (!EmailValidator.validate(value.trim())) {
          return 'Invalid email format';
        }

        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Password",
      isPassword: true,
      textInputAction: TextInputAction.done,
      focusNode: FocusNode(),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Please enter your password';
        } else if (value.length < 8) {
          return 'Password must be at least 8 characters';
        }

        return null;
      },
    ),
  ];

  static final List<TextFieldEntity> authForgotPassword = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Email",
      keyboardType: TextInputType.emailAddress,
      focusNode: FocusNode(),
      textInputAction: TextInputAction.done,
      validator: (value) {
        if (value == null || value.trim().isEmpty) {
          return 'Please enter your email';
        } else if (!EmailValidator.validate(value.trim())) {
          return 'Invalid email format';
        }

        return null;
      },
    ),
  ];

  static final List<TextFieldEntity> verificationOTP = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "",
      keyboardType: TextInputType.number,
      focusNode: FocusNode(),
      textInputAction: TextInputAction.next,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      validator: (value) {
        if (value == null || value.isEmpty) {
          return '*';
        }
        if (value.length > 1) {
          return 'Max 1';
        }

        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "",
      keyboardType: TextInputType.number,
      focusNode: FocusNode(),
      textInputAction: TextInputAction.next,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      validator: (value) {
        if (value == null || value.isEmpty) {
          return '*';
        }
        if (value.length > 1) {
          return 'Max 1';
        }

        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "",
      keyboardType: TextInputType.number,
      focusNode: FocusNode(),
      textInputAction: TextInputAction.next,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      validator: (value) {
        if (value == null || value.isEmpty) {
          return '*';
        }
        if (value.length > 1) {
          return 'Max 1';
        }

        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "",
      keyboardType: TextInputType.number,
      focusNode: FocusNode(),
      textInputAction: TextInputAction.done,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      validator: (value) {
        if (value == null || value.isEmpty) {
          return '*';
        }
        if (value.length > 1) {
          return 'Max 1';
        }

        return null;
      },
    ),
  ];

  static final List<TextFieldEntity> updateProfile = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Username",
      label: "Username",
      keyboardType: TextInputType.name,
      focusNode: FocusNode(),
      isEnabled: false,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Name",
      label: "Name",
      keyboardType: TextInputType.name,
      focusNode: FocusNode(),
      validator: (value) {
        if (value == null || value.trim().isEmpty) {
          return 'Name required';
        }

        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Phone Number",
      label: "Phone Number",
      keyboardType: TextInputType.phone,
      focusNode: FocusNode(),
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      validator: (value) {
        if (value != null || value!.trim().isEmpty) {
          if (!RegExp(r'^0.+(\d{9,12})$').hasMatch(value)) {
            return 'Please input a valid phone number, starting with 0';
          }
        }

        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Address",
      label: "Address",
      focusNode: FocusNode(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Birthplace",
      label: "Birthplace",
      focusNode: FocusNode(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Birthdate",
      label: "Birthdate",
      isEnabled: false,
      focusNode: FocusNode(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Instagram",
      label: "Instagram",
      focusNode: FocusNode(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Facebook",
      label: "Facebook",
      focusNode: FocusNode(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Tiktok",
      label: "Tiktok",
      textInputAction: TextInputAction.done,
      focusNode: FocusNode(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Nickname",
      label: "Nickname",
      keyboardType: TextInputType.name,
      focusNode: FocusNode(),
    ),
  ];

  static final List<TextFieldEntity> requestEventForm = [
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "Event Name",
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Event Name Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "Description",
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Description Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "Event PIC",
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Event PIC Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "Phone Number",
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Phone Number Required';
        }
        return null;
      },
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "Date",
      isEnabled: false,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Date Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "Start Time",
      isEnabled: false,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Start Time Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "End Time",
      isEnabled: false,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'End Time Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "Location",
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Location Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "City",
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'City Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '1.300.000',
      ),
      hint: "",
      label: "Price",
      isEnabled: false,
    ),
    TextFieldEntity(
      textController: TextEditingController(
        text: '',
      ),
      hint: "",
      label: "Negotiation Price",
      textInputAction: TextInputAction.done,
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
    ),
  ];

  static final List<TextFieldEntity> changePassword = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter New Password",
      focusNode: FocusNode(),
      label: "New Password".toUpperCase(),
      isPassword: true,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Confirm Password",
      focusNode: FocusNode(),
      label: "Confirm Password".toUpperCase(),
      isPassword: true,
      textInputAction: TextInputAction.done,
    ),
  ];

  static final List<TextFieldEntity> creditCard = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "1234 5678 9101",
      focusNode: FocusNode(),
      label: "Nomor Kartu",
      keyboardType: TextInputType.number,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "John Doe",
      focusNode: FocusNode(),
      label: "Nama Pemilik Kartu",
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "MM",
      focusNode: FocusNode(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "YYYY",
      focusNode: FocusNode(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "CVV",
      focusNode: FocusNode(),
      textInputAction: TextInputAction.done,
    ),
  ];

  static final List<TextFieldEntity> resetPassword = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter New Password",
      focusNode: FocusNode(),
      // label: "New Password",
      isPassword: true,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Enter Confirm Password",
      focusNode: FocusNode(),
      // label: "Confirm Password",
      isPassword: true,
      textInputAction: TextInputAction.done,
    ),
  ];

  static final List<TextFieldEntity> createOfferService = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Name of Service",
      focusNode: FocusNode(),
      label: "Name Of Service".toUpperCase(),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Name of Service Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Description of Service",
      focusNode: FocusNode(),
      label: "Description Of Service".toUpperCase(),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Description of Service Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Certification",
      focusNode: FocusNode(),
      label: "Certification".toUpperCase(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Award / Recognition",
      focusNode: FocusNode(),
      label: "Award / Recognition".toUpperCase(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Fixed Fee",
      focusNode: FocusNode(),
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
      label: "Fixed Fee".toUpperCase(),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Fixed Fee Required';
        }
        return null;
      },
    ),
  ];

  static final List<TextFieldEntity> feeOfferService = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Price of (Photo/Video/State)",
      focusNode: FocusNode(),
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
      label: "Price of (Photo / Video / State)".toUpperCase(),
      validator: (value) {
        if (value == null || value.isEmpty) {
          return 'Price Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input % Paid to Other User",
      focusNode: FocusNode(),
      label: "% Paid to Other User".toUpperCase(),
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      keyboardType: TextInputType.number,
      validator: (value) {
        if (value == null || value.isEmpty) {
          return '% Paid Required';
        } else if (int.parse(value.toString()) > 100) {
          return '% Paid Must Be Under or Equal 100%';
        }
        return null;
      },
    ),
  ];

  static final TextFieldEntity minHourService = TextFieldEntity(
    textController: TextEditingController(text: ''),
    hint: "Input Hour",
    inputFormatters: <TextInputFormatter>[
      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
    ],
    keyboardType: TextInputType.number,
  );

  static final TextFieldEntity maxHourService = TextFieldEntity(
    textController: TextEditingController(text: ''),
    hint: "Input Hour",
    inputFormatters: <TextInputFormatter>[
      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
    ],
    keyboardType: TextInputType.number,
  );

  static final TextFieldEntity fixedFeeService = TextFieldEntity(
    textController: TextEditingController(text: ''),
    hint: "Input Price",
    inputFormatters: <TextInputFormatter>[
      FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      CurrencyTextInputFormatter(
        symbol: "Rp ",
        decimalDigits: 0,
      ),
    ],
    keyboardType: TextInputType.number,
  );

  static final List<TextFieldEntity> fixedFeePerMonth = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Price",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Max. Event",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      keyboardType: TextInputType.number,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Extra Event Price",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.done,
    ),
  ];

  static final List<TextFieldEntity> fixedFeePerHalfMonth = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Price",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Max. Event",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      keyboardType: TextInputType.number,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Extra Event Price",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Extra Event Price",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.done,
    ),
  ];

  static final List<TextFieldEntity> fixedFeePerDay = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Price",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Price",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Hour",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      keyboardType: TextInputType.number,
      textInputAction: TextInputAction.done,
    ),
  ];

  static final List<TextFieldEntity> feeSalesModel = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Price",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
        CurrencyTextInputFormatter(
          symbol: "Rp ",
          decimalDigits: 0,
        ),
      ],
      keyboardType: TextInputType.number,
      focusNode: FocusNode(),
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      label: "VARIABLE FEE",
      hint: "Input Variable (%)",
      inputFormatters: <TextInputFormatter>[
        FilteringTextInputFormatter.allow(RegExp(r'[0-9]')),
      ],
      keyboardType: TextInputType.number,
      focusNode: FocusNode(),
    )
  ];

  static final List<TextFieldEntity> serviceContract = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      label: "Start Contract".toUpperCase(),
      hint: "Select Date",
      focusNode: FocusNode(),
      isEnabled: false,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      label: "End Contract".toUpperCase(),
      hint: "Select Date",
      textInputAction: TextInputAction.done,
      focusNode: FocusNode(),
      isEnabled: false,
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      label: "Date Contract".toUpperCase(),
      hint: "Select Date",
      textInputAction: TextInputAction.done,
      focusNode: FocusNode(),
      isEnabled: false,
    ),
  ];

  /// index 0 = event,
  /// 1 = date,
  /// 2 = start time,
  /// 3 = end time,
  /// 4 = location
  static final List<TextFieldEntity> newCreatePublicEventRequest = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Event",
      focusNode: FocusNode(),
      label: "Event".toUpperCase(),
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Event Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Select Date",
      focusNode: FocusNode(),
      label: "Date".toUpperCase(),
      isEnabled: false,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Date Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "_ _ : _ _",
      focusNode: FocusNode(),
      label: "Start Time".toUpperCase(),
      isEnabled: false,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Start Time Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "_ _ : _ _",
      focusNode: FocusNode(),
      label: "End Time".toUpperCase(),
      isEnabled: false,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'End Time Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Location",
      focusNode: FocusNode(),
      label: "Location".toUpperCase(),
      textInputAction: TextInputAction.done,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Location Required';
        }
        return null;
      },
    ),
  ];

  static final TextFieldEntity noteRequestService = TextFieldEntity(
    textController: TextEditingController(text: ''),
    hint: "",
    focusNode: FocusNode(),
    textInputAction: TextInputAction.done,
    label: "Notes".toUpperCase(),
  );

  static final List<TextFieldEntity> requestExistingEvent = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Select Date",
      focusNode: FocusNode(),
      label: "Date".toUpperCase(),
      isEnabled: false,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Date Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "_ _ : _ _",
      focusNode: FocusNode(),
      label: "Start Time".toUpperCase(),
      isEnabled: false,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Start Time Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "_ _ : _ _",
      focusNode: FocusNode(),
      label: "End Time".toUpperCase(),
      isEnabled: false,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'End Time Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Location",
      focusNode: FocusNode(),
      isEnabled: false,
      label: "Location".toUpperCase(),
      textInputAction: TextInputAction.done,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Location Required';
        }
        return null;
      },
    ),
  ];

  static final List<TextFieldEntity> requestNewSingleEvent = [
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Event",
      focusNode: FocusNode(),
      label: "Event".toUpperCase(),
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Event Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Select Date",
      focusNode: FocusNode(),
      label: "Date".toUpperCase(),
      isEnabled: false,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Date Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "_ _ : _ _",
      focusNode: FocusNode(),
      label: "Start Time".toUpperCase(),
      isEnabled: false,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Start Time Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "_ _ : _ _",
      focusNode: FocusNode(),
      label: "End Time".toUpperCase(),
      isEnabled: false,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'End Time Required';
        }
        return null;
      },
    ),
    TextFieldEntity(
      textController: TextEditingController(text: ''),
      hint: "Input Location",
      focusNode: FocusNode(),
      label: "Location".toUpperCase(),
      textInputAction: TextInputAction.done,
      validator: (dt) {
        if (dt == null || dt.isEmpty) {
          return 'Location Required';
        }
        return null;
      },
    ),
  ];
}
