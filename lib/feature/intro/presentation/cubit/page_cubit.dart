import 'package:bloc/bloc.dart';

class PageCubit extends Cubit<int> {
  PageCubit() : super(0);

  void goToLoginPage() => emit(0);
  void goToSignUpPages() => emit(1);
  void goToForgotPasswordPage() => emit(2);
  void goToVerivicationPage() => emit(3);
  void goToResetPasswordPage() => emit(4);
}
