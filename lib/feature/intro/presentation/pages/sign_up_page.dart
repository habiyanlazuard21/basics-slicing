import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../lib.dart';

class SignUpPage extends StatefulWidget {
  const SignUpPage({super.key});

  @override
  State<SignUpPage> createState() => _SignUpPageState();
}

class _SignUpPageState extends State<SignUpPage> {
  final TextEditingController _emailController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.9,
      decoration: const BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      padding: const EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.close,
                color: AppColors.grey,
              ),
            ),
          ),
          const Gap(height: AppGap.large),

          /* logo */
          const Center(
            child: CustomImageWrapper(
              image: AppIcon.logoMain,
              width: 96,
              height: 96,
              fit: BoxFit.cover,
              isNetworkImage: false,
            ),
          ),
          const Gap(height: AppGap.big + AppGap.tiny),

          /* title */
          Text(
            "Sign Up",
            style: AppTextStyle.semiBold.copyWith(
              color: AppColors.black,
              fontSize: AppFontSize.extraLarge,
            ),
          ),
          const Gap(height: AppGap.big + AppGap.tiny),

          /* email field */
          CustomTextFormField(
            textFieldEntity: TextFieldEntity(
              textController: _emailController,
              hint: "Email/Phone Number",
            ),
          ),
          const Gap(height: AppGap.normal),
          CustomTextFormField(
            backgroundDisable: AppColors.black,
            textFieldEntity: TextFieldEntity(
              textController: _emailController,
              hint: "Password",
              isPassword: true,
            ),
          ),
          const Gap(height: AppGap.extraLarge),
          ButtonPrimary(
            "Continue",
            onPressed: () {
              //TODO: need funtion
            },
            height: 43,
            width: double.infinity,
            buttonColor: AppColors.black,
            fontSize: AppFontSize.medium,
          ),
          const Gap(height: AppGap.big + AppGap.tiny),
          Row(
            children: [
              Text(
                "Already have an account?",
                style: AppTextStyle.regular.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.normal,
                ),
              ),
              GestureDetector(
                onTap: () {
                  context.read<PageCubit>().goToLoginPage();
                },
                child: Text(
                  " Login",
                  style: AppTextStyle.semiBold.copyWith(
                    color: AppColors.secondary,
                    fontSize: AppFontSize.normal,
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
