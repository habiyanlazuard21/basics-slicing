import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../lib.dart';

class ForgotPasswordPage extends StatelessWidget {
  const ForgotPasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    final TextEditingController _emailController = TextEditingController();
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.9,
      decoration: const BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: Column(
        children: [
          const Gap(height: AppGap.big + AppGap.tiny),
          CustomAppBar(
            title: 'Forgot Password',
            elevation: 0,
            onPressedBack: () {
              context.read<PageCubit>().goToLoginPage();
            },
          ),
          ColumnPadding(
            crossAxisAlignment: CrossAxisAlignment.start,
            padding: const EdgeInsets.symmetric(
              horizontal: AppGap.extraLarge,
            ),
            children: [
              const Gap(height: AppGap.large),
              const Center(
                child: CustomImageWrapper(
                  image: AppIcon.logoMain,
                  width: 96,
                  height: 96,
                  fit: BoxFit.cover,
                  isNetworkImage: false,
                ),
              ),
              const Gap(height: AppGap.big + AppGap.tiny),
              CustomTextFormField(
                backgroundDisable: AppColors.black,
                textFieldEntity: TextFieldEntity(
                  textController: _emailController,
                  hint: "Email/Phone Number",
                ),
              ),
              const Gap(height: AppGap.extraLarge),
              ButtonPrimary(
                "Continue",
                onPressed: () {
                  //TODO: need funtion
                  context.read<PageCubit>().goToVerivicationPage();
                },
                height: 43,
                width: double.infinity,
                buttonColor: AppColors.black,
                labelColor: AppColors.white,
                fontSize: AppFontSize.medium,
              ),
            ],
          ),
        ],
      ),
    );
  }
}
