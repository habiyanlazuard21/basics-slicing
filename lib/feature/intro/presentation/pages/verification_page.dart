import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../lib.dart';

class VerificationPage extends StatelessWidget {
  const VerificationPage({super.key});

  @override
  Widget build(BuildContext context) {
    final List<TextFieldEntity> _verificationOtpEntity =
        TextFieldEntity.verificationOTP;
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.9,
      decoration: const BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          const Gap(height: AppGap.big + AppGap.tiny),
          CustomAppBar(
            title: 'Verification',
            elevation: 0,
            onPressedBack: () {
              context.read<PageCubit>().goToForgotPasswordPage();
            },
          ),
          ColumnPadding(
            padding: const EdgeInsets.symmetric(
              horizontal: AppGap.extraLarge,
            ),
            children: [
              const Gap(height: AppGap.large),
              const Center(
                child: CustomImageWrapper(
                  image: AppIcon.logoMain,
                  width: 96,
                  height: 96,
                  fit: BoxFit.cover,
                  isNetworkImage: false,
                ),
              ),
              const Gap(height: AppGap.small),
              Center(
                child: Text(
                  "Enter the OTP code sent to your email",
                  style: AppTextStyle.regular.copyWith(
                    color: AppColors.black,
                    fontSize: AppFontSize.normal,
                  ),
                ),
              ),
              const Gap(height: AppGap.medium + AppGap.tiny),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Flexible(
                    flex: 1,
                    child: CustomTextFormFieldOtp(
                      textFieldEntity: _verificationOtpEntity[0],
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).requestFocus(
                              _verificationOtpEntity[1].focusNode);
                        }
                      },
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: CustomTextFormFieldOtp(
                      textFieldEntity: _verificationOtpEntity[1],
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).requestFocus(
                              _verificationOtpEntity[2].focusNode);
                        }
                        if (value.isEmpty) {
                          FocusScope.of(context).requestFocus(
                              _verificationOtpEntity[0].focusNode);
                        }
                      },
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: CustomTextFormFieldOtp(
                      textFieldEntity: _verificationOtpEntity[2],
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).requestFocus(
                              _verificationOtpEntity[3].focusNode);
                        }
                        if (value.isEmpty) {
                          FocusScope.of(context).requestFocus(
                              _verificationOtpEntity[1].focusNode);
                        }
                      },
                    ),
                  ),
                  Flexible(
                    flex: 1,
                    child: CustomTextFormFieldOtp(
                      textFieldEntity: _verificationOtpEntity[3],
                      onChanged: (value) {
                        if (value.length == 1) {
                          FocusScope.of(context).unfocus();
                        }
                        if (value.isEmpty) {
                          FocusScope.of(context).requestFocus(
                              _verificationOtpEntity[2].focusNode);
                        }
                      },
                    ),
                  ),
                ],
              ),
              const Gap(height: AppGap.medium + AppGap.tiny),
              ButtonPrimary(
                "Continue",
                onPressed: () {
                  //TODO: need funtion
                  context.read<PageCubit>().goToResetPasswordPage();
                },
                height: 43,
                width: double.infinity,
                buttonColor: AppColors.black,
                fontSize: AppFontSize.medium,
                isButtonDisabled: false,
              ),
              const Gap(height: AppGap.medium + AppGap.tiny),
              ClickableText(
                onTap: () {},
              )
            ],
          ),
        ],
      ),
    );
  }
}

class ClickableText extends StatelessWidget {
  final VoidCallback onTap;

  const ClickableText({Key? key, required this.onTap}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      padding: const EdgeInsets.symmetric(horizontal: 16.0),
      child: RichText(
        textAlign: TextAlign.center,
        text: TextSpan(
          children: [
            const TextSpan(
              text: "Please ",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            TextSpan(
              text: "Click Here",
              style: const TextStyle(
                decoration: TextDecoration.underline,
                color: Colors.blue,
              ),
              recognizer: TapGestureRecognizer()..onTap = onTap,
            ),
            const TextSpan(
              text:
                  " if you don't receive the OTP. \nThen repeat your previous process.",
              style: TextStyle(
                color: Colors.black,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
