import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../lib.dart';

class ResetPasswordPage extends StatelessWidget {
  const ResetPasswordPage({super.key});

  @override
  Widget build(BuildContext context) {
    final TextEditingController _emailController = TextEditingController();
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.9,
      decoration: const BoxDecoration(
        color: AppColors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(20),
          topRight: Radius.circular(20),
        ),
      ),
      child: ColumnPadding(
        crossAxisAlignment: CrossAxisAlignment.start,
        padding: const EdgeInsets.only(
          top: AppGap.big + AppGap.tiny,
          left: AppGap.extraLarge,
          right: AppGap.extraLarge,
        ),
        children: [
          const CustomAppBar(
            title: 'Reset Password',
            automaticallyImplyLeading: false,
            elevation: 0,
          ),
          const Gap(height: AppGap.large),
          const Center(
            child: CustomImageWrapper(
              image: AppIcon.logoMain,
              width: 96,
              height: 96,
              fit: BoxFit.cover,
              isNetworkImage: false,
            ),
          ),
          const Gap(height: AppGap.big + AppFontSize.tiny),
          CustomTextFormField(
            backgroundDisable: AppColors.black,
            textFieldEntity: TextFieldEntity(
              textController: _emailController,
              hint: "Password",
              isPassword: true,
            ),
          ),
          const Gap(height: AppGap.normal),
          CustomTextFormField(
            backgroundDisable: AppColors.black,
            textFieldEntity: TextFieldEntity(
              textController: _emailController,
              hint: "Password",
              isPassword: true,
            ),
          ),
          const Gap(height: AppGap.extraLarge),
          ButtonPrimary(
            "Submit",
            onPressed: () {
              //TODO: need funtion
              context.read<PageCubit>().goToLoginPage();
            },
            height: 43,
            width: double.infinity,
            buttonColor: AppColors.black,
            labelColor: AppColors.white,
            fontSize: AppFontSize.medium,
          ),
        ],
      ),
    );
  }
}
