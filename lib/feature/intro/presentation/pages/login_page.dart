import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../lib.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _emailController = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      height: MediaQuery.of(context).size.height * 0.9,
      padding: const EdgeInsets.all(15.0),
      decoration: const BoxDecoration(
          color: AppColors.white,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(20),
            topRight: Radius.circular(20),
          )),
      child: ColumnPadding(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Align(
            alignment: Alignment.topRight,
            child: IconButton(
              onPressed: () {
                Navigator.pop(context);
              },
              icon: const Icon(
                Icons.close,
                color: AppColors.grey,
              ),
            ),
          ),
          const Gap(height: AppGap.large),
          const Center(
            child: CustomImageWrapper(
              image: AppIcon.logoMain,
              width: 96,
              height: 96,
              fit: BoxFit.cover,
              isNetworkImage: false,
            ),
          ),
          const Gap(height: AppGap.large + AppGap.normal),
          Text(
            "Login",
            style: AppTextStyle.semiBold.copyWith(
              color: AppColors.black,
              fontSize: AppFontSize.extraLarge,
            ),
          ),
          const Gap(height: AppGap.large + AppGap.normal),
          CustomTextFormField(
            backgroundDisable: AppColors.black,
            textFieldEntity: TextFieldEntity(
              textController: _emailController,
              hint: "Email/Phone Number",
            ),
          ),
          const Gap(height: AppGap.normal),
          CustomTextFormField(
            backgroundDisable: AppColors.black,
            textFieldEntity: TextFieldEntity(
              textController: _emailController,
              hint: "Password",
              isPassword: true,
            ),
          ),
          const Gap(height: AppGap.extraLarge),
          ButtonPrimary(
            "Continue",
            onPressed: () {
              //TODO: need funtion
            },
            height: 43,
            width: double.infinity,
            buttonColor: AppColors.black,
            fontSize: AppFontSize.medium,
          ),
          TextButton(
            onPressed: () {
              context.read<PageCubit>().goToForgotPasswordPage();
            },
            child: Text(
              "Forgot Password?",
              style: AppTextStyle.medium.copyWith(
                color: AppColors.secondary,
                fontSize: AppFontSize.normal,
              ),
            ),
          ),
          Row(
            children: [
              Text(
                "  Create your free account.",
                style: AppTextStyle.regular.copyWith(
                  color: AppColors.black,
                  fontSize: AppFontSize.normal,
                ),
              ),
              TextButton(
                onPressed: () {
                  context.read<PageCubit>().goToSignUpPages();
                },
                child: Text(
                  " Sign up",
                  style: AppTextStyle.semiBold.copyWith(
                    color: AppColors.secondary,
                    fontSize: AppFontSize.normal,
                  ),
                ),
              )
            ],
          )
        ],
      ),
    );
  }
}
