import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../lib.dart';

class IntroPage extends StatelessWidget {
  const IntroPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          const CustomImageWrapper(
            image: AppImages.intro,
            borderRadius: 0,
            width: double.infinity,
            height: double.infinity,
            isNetworkImage: false,
          ),

          /* label skip  */
          Positioned(
            top: MediaQuery.of(context).viewPadding.top + AppGap.medium,
            right: AppGap.large,
            child: GestureDetector(
              onTap: () {
                Navigator.pushNamedAndRemoveUntil(
                  context,
                  PagePath.bottomNavBar,
                  (route) => false,
                );
              },
              child: Text(
                "Skip",
                style: AppTextStyle.medium.copyWith(
                  color: AppColors.white,
                  fontSize: AppFontSize.medium,
                ),
              ),
            ),
          ),

          /* logo  */
          Container(
            alignment: Alignment.topCenter,
            padding: EdgeInsets.only(
              top: MediaQuery.of(context).viewPadding.top + AppGap.extraBig,
            ),
            child: Image.asset(
              AppIcon.logo,
              width: 96,
              height: 96,
              filterQuality: FilterQuality.low,
            ),
          ),

          /* content  */
          Container(
            margin: const EdgeInsets.only(
              top: 485,
              left: AppFontSize.extraLarge,
              right: AppBorderRadius.extraLarge,
            ),
            height: ResponsiveUtils(context).getMediaQueryHeight() < 825
                ? 340
                : ResponsiveUtils(context).getMediaQueryHeight() - 485,
            width: double.infinity,
            child: _bottomContent(context),
          ),
        ],
      ),
    );
  }

  ColumnPadding _bottomContent(BuildContext context) {
    const String _descIntro =
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Condimentum dolor vitae aenean sollicitudin augue augue. Eget.';
    return ColumnPadding(
      children: [
        Text(
          "Easy access to",
          style: AppTextStyle.bold.copyWith(
            color: AppColors.white,
            fontSize: AppFontSize.medium,
          ),
        ),
        const Gap(height: AppGap.small),
        Text(
          "Images and videos",
          style: AppTextStyle.bold.copyWith(
            color: AppColors.white,
            fontSize: AppFontSize.big,
          ),
        ),
        const Gap(height: AppGap.small),
        Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20),
          child: Text(
            _descIntro,
            style: AppTextStyle.medium.copyWith(
              color: AppColors.white,
              fontSize: AppFontSize.small,
            ),
            textAlign: TextAlign.center,
          ),
        ),
        const Gap(height: AppGap.medium),

        /* button sign up  */
        ButtonPrimary(
          "Sign Up",
          onPressed: () {
            //TODO: need funtion
            showModalBottomSheet(
              backgroundColor: Colors.transparent,
              context: context,
              isScrollControlled: true,
              builder: (context) {
                return const SignUpPage();
              },
            );
          },
          width: double.infinity,
          fontSize: AppFontSize.medium,
          height: 43,
        ),
        const Gap(height: AppGap.normal),

        /* button login  */
        ButtonPrimary(
          "Login",
          onPressed: () {
            //TODO: need funtion
            showModalBottomSheet(
              backgroundColor: Colors.transparent,
              isScrollControlled: true,
              context: context,
              builder: (context) {
                return BlocProvider(
                  create: (context) => PageCubit(),
                  child: const _RoutePagesAuth(),
                );
              },
            );
          },
          width: double.infinity,
          borderColor: AppColors.main,
          buttonColor: AppColors.transparent,
          labelColor: AppColors.main,
          fontSize: AppFontSize.medium,
          elevation: 0,
          height: 43,
        ),
        const Spacer()
      ],
    );
  }
}

class _RoutePagesAuth extends StatelessWidget {
  const _RoutePagesAuth();

  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PageCubit, int>(
      builder: (context, state) {
        switch (state) {
          case 0:
            return const LoginPage();
          case 1:
            return const SignUpPage();
          case 2:
            return const ForgotPasswordPage();
          case 3:
            return const VerificationPage();
          case 4:
            return const ResetPasswordPage();
          default:
            return Container();
        }
      },
    );
  }
}
