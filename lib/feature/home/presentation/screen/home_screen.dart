import 'package:flutter/material.dart';

import '../../../../lib.dart';

class HomeScreen extends StatefulWidget {
  final HomeScreenParams _params;
  const HomeScreen({
    Key? key,
    required HomeScreenParams params,
  })  : _params = params,
        super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController _seacrhController = TextEditingController();
  final List<DetailImageEventEntity> _recentItem =
      DetailImageEventEntity.getRecentItem;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [
            /* App Header */
            ColumnPadding(
              crossAxisAlignment: CrossAxisAlignment.start,
              padding: const EdgeInsets.symmetric(
                horizontal: AppFontSize.extraBig,
                vertical: AppFontSize.big / 2,
              ),
              children: [
                Text(
                  "Recent Events",
                  style: AppTextStyle.bold.copyWith(
                    color: AppColors.black,
                    fontSize: AppFontSize.extraLarge,
                  ),
                ),
                const Gap(height: AppGap.large),

                /* search */
                CustomTextFormFieldSearch(
                  controller: _seacrhController,
                  onSubmitted: (p0) {},
                  onPressed: () {},
                ),
              ],
            ),

            /* conntent */
            Expanded(
              child: ListView.separated(
                itemBuilder: (context, index) {
                  return CustomInkwell(
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        PagePath.detailEventPage,
                        arguments: DetailEventParams(
                          image: _recentItem[index].images,
                          title: _recentItem[index].title,
                          subtitle: _recentItem[index].subtitle,
                          desc: _recentItem[index].desc,
                          date: _recentItem[index].date,
                        ),
                      );
                    },
                    child: ColumnPadding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: AppFontSize.extraBig,
                      ),
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        const Gap(height: AppGap.small),

                        /* image item */
                        CustomImageWrapper(
                          image: _recentItem[index].images,
                          width: double.infinity,
                          height: 180,
                          isNetworkImage: false,
                        ),
                        const Gap(height: AppGap.small),

                        /* title item */
                        Text(
                          _recentItem[index].title,
                          style: AppTextStyle.semiBold.copyWith(
                            color: AppColors.black,
                            fontSize: AppFontSize.extraLarge,
                          ),
                        ),
                        const Gap(height: AppGap.small),

                        /* subtitle item */
                        Text(
                          _recentItem[index].subtitle,
                          style: AppTextStyle.semiBold.copyWith(
                            color: AppColors.black,
                            fontSize: AppFontSize.medium,
                          ),
                        ),
                        const Gap(height: AppGap.small),

                        /* date item */
                        Text(
                          _recentItem[index].date,
                          style: AppTextStyle.semiBold.copyWith(
                            color: AppColors.black,
                            fontSize: AppFontSize.medium,
                          ),
                        ),
                        const Gap(height: AppGap.small)
                      ],
                    ),
                  );
                },
                separatorBuilder: (context, index) => const Divider(
                  thickness: 3,
                  color: AppColors.greyShade,
                  height: AppGap.small,
                  indent: AppFontSize.extraBig,
                  endIndent: AppFontSize.extraBig,
                ),
                itemCount: _recentItem.length,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
