class HomeScreenParams {
  final bool isFromDiscover;
  String search;
  final String filterEventType;
  final String filterCity;
  final String filterMonth;
  final String filterEventGroup;

  HomeScreenParams({
    this.isFromDiscover = false,
    this.search = "",
    this.filterEventType = "",
    this.filterCity = "",
    this.filterMonth = "",
    this.filterEventGroup = "",
  });
}
