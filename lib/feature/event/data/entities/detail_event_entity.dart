import '../../../../lib.dart';

class DetailEventEntity {
  final String images;
  final String title;
  final String count;

  DetailEventEntity(this.images, this.title, this.count);

  static List<DetailEventEntity> getItemNewHome = [
    DetailEventEntity(AppImages.basketball, "Chapalla 21 Dunk", "12"),
    DetailEventEntity(AppImages.basketball, "Lapangan Baru Standfield", "12"),
    DetailEventEntity(AppImages.basketball, "Chapalla 21 Dunk", "12"),
    DetailEventEntity(AppImages.basketball, "Chapalla 21 Dunk", "12"),
    DetailEventEntity(AppImages.basketball, "Chapalla 21 Dunk", "12"),
    DetailEventEntity(AppImages.basketball, "Chapalla 21 Dunk", "12"),
  ];
}
