import '../../../../lib.dart';

class DetailImageEventEntity {
  final String images;
  final String title;
  final String subtitle;
  final String desc;
  final String date;

  DetailImageEventEntity(
    this.images,
    this.title,
    this.subtitle,
    this.desc,
    this.date,
  );

  static List<String> getItemKeyword = [
    'Soccer',
    'Video',
    'Playing',
    'Ball',
    'Competition',
  ];

  static List<DetailImageEventEntity> getRecentItem = [
    DetailImageEventEntity(
      AppImages.town1,
      "Basketball Junior drums Leaguage Basketball Competition 2022",
      "Pertandingan Chapalla vs Phoenix",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      "July 5, 2022",
    ),
    DetailImageEventEntity(
      AppImages.itemRecentImage,
      "Basketball Junior drums Leaguage Basketball Competition 2022",
      "Pertandingan Chapalla vs Phoenix",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      "July 5, 2022",
    ),
    DetailImageEventEntity(
      AppImages.itemRecentImage,
      "Basketball Junior drums Leaguage Basketball Competition 2022",
      "Pertandingan Chapalla vs Phoenix",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      "July 5, 2022",
    ),
    DetailImageEventEntity(
      AppImages.itemRecentImage,
      "Basketball Junior drums Leaguage Basketball Competition 2022",
      "Pertandingan Chapalla vs Phoenix",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      "July 5, 2022",
    ),
    DetailImageEventEntity(
      AppImages.itemRecentImage,
      "Basketball Junior drums Leaguage Basketball Competition 2022",
      "Pertandingan Chapalla vs Phoenix",
      "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
      "July 5, 2022",
    ),
  ];
}
