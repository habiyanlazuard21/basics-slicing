class DetailImageEventParams {
  final String image;
  final String title;
  final String subTitle;

  DetailImageEventParams({
    this.image = "",
    this.title = "",
    this.subTitle = "",
  });
}
