class DetailEventParams {
  final String image;
  final String title;
  final String subtitle;
  final String desc;
  final String date;

  DetailEventParams({
    this.image = "",
    this.title = "",
    this.subtitle = "",
    this.desc = "",
    this.date = "",
  });
}
