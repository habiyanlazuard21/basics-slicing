import 'package:flutter/material.dart';

import '../../../../lib.dart';

class DetailEventPage extends StatefulWidget {
  const DetailEventPage({
    Key? key,
    required DetailEventParams params,
  })  : _params = params,
        super(key: key);

  final DetailEventParams _params;

  @override
  State<DetailEventPage> createState() => _DetailEventPageState();
}

class _DetailEventPageState extends State<DetailEventPage> {
  final List<DetailEventEntity> _newHomeItem = DetailEventEntity.getItemNewHome;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /* App Bar */
            Padding(
              padding: const EdgeInsets.only(right: AppGap.normal),
              child: CustomAppBar(
                title: widget._params.subtitle,
                elevation: 0,
                onPressedBack: () {
                  Navigator.pop(context);
                },
                onTapShare: () {},
              ),
            ),
            const Gap(height: AppGap.small),

            /* Content Body */
            Expanded(
              child: CustomSingleChildScrollViewWrapper(
                child: ColumnPadding(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  padding: const EdgeInsets.symmetric(
                    horizontal: AppGap.large,
                  ),
                  children: [
                    const Gap(height: AppGap.small),

                    /* title youtube */
                    Text(
                      "Link Youtube",
                      style: AppTextStyle.semiBold.copyWith(
                        color: AppColors.black,
                        fontSize: AppFontSize.medium,
                      ),
                    ),
                    const Gap(height: AppGap.small),

                    /* Image youtube */
                    Stack(
                      alignment: Alignment.center,
                      children: [
                        CustomImageWrapper(
                          image: widget._params.image,
                          isNetworkImage: false,
                          width: double.infinity,
                          borderRadius: AppBorderRadius.medium,
                          height: 214,
                        ),

                        /* Icon Button Play */
                        GestureDetector(
                          onTap: () {},
                          child: Container(
                            width: 46,
                            height: 46,
                            decoration: const BoxDecoration(
                              color: AppColors.white,
                              shape: BoxShape.circle,
                            ),
                            child: const Icon(
                              Icons.play_arrow,
                              size: AppFontSize.large,
                              color: AppColors.black,
                            ),
                          ),
                        )
                      ],
                    ),
                    const Gap(height: AppGap.medium),

                    /* title detail item */
                    Text(
                      widget._params.subtitle,
                      style: AppTextStyle.semiBold.copyWith(
                        color: AppColors.black,
                        fontSize: AppFontSize.medium,
                      ),
                    ),
                    const Gap(height: AppGap.small),

                    /* desc detail item */
                    Text(
                      widget._params.desc,
                      style: AppTextStyle.medium.copyWith(
                        color: AppColors.black,
                        fontSize: AppFontSize.normal,
                      ),
                    ),
                    const Gap(
                      height: AppGap.extraLarge + 5,
                    ),

                    /* filter */
                    ButtonFilter(
                      onPressed: () {},
                      buttonColor: AppColors.transparent,
                      width: 84,
                      height: AppButtonSize.normal - 6,
                    ),
                    const Gap(height: AppGap.medium),

                    /* List item */
                    GridView.builder(
                      gridDelegate:
                          const SliverGridDelegateWithFixedCrossAxisCount(
                        crossAxisCount: 2,
                        childAspectRatio: 178 / 300,
                        crossAxisSpacing: 16,
                        mainAxisSpacing: 16,
                      ),
                      shrinkWrap: true,
                      physics: const NeverScrollableScrollPhysics(),
                      itemCount: _newHomeItem.length,
                      itemBuilder: (context, index) {
                        /* card item */
                        return _DetailEventItem(
                          images: _newHomeItem[index].images,
                          title: _newHomeItem[index].title,
                          count: _newHomeItem[index].count,
                          onTap: () {
                            Navigator.pushNamed(
                              context,
                              PagePath.detailImageEventPage,
                              arguments: DetailImageEventParams(
                                image: _newHomeItem[index].images,
                                title: _newHomeItem[index].title,
                                subTitle: widget._params.title,
                              ),
                            );
                          },
                        );
                      },
                    ),
                    const Gap(height: AppGap.extraLarge)
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _DetailEventItem extends StatelessWidget {
  const _DetailEventItem({
    required this.images,
    required this.title,
    required this.count,
    required this.onTap,
  });

  final String images;
  final String title;
  final String count;
  final Function() onTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(AppBorderRadius.small),
            color: AppColors.white,
            boxShadow: const [
              BoxShadow(
                offset: Offset(0, 4),
                blurRadius: 12,
                spreadRadius: 0,
                color: AppColors.grey,
              )
            ]),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /* image item list */
            Stack(
              children: [
                CustomImageWrapper(
                  image: images,
                  width: double.infinity,
                  height: ResponsiveUtils(context).getMediaQueryHeight() / 3.5,
                  borderRadius: AppBorderRadius.small,
                  fit: BoxFit.cover,
                  isNetworkImage: false,
                ),

                /* icon person item list */
                Positioned(
                  left: AppGap.small,
                  top: AppBorderRadius.small,
                  child: GestureDetector(
                    onTap: () {},
                    child: Container(
                      width: 32,
                      height: 32,
                      decoration: const BoxDecoration(
                        color: AppColors.white,
                        shape: BoxShape.circle,
                      ),
                      child: const Icon(
                        Icons.person_outline_rounded,
                        size: AppFontSize.small,
                        color: AppColors.black,
                      ),
                    ),
                  ),
                ),

                /* icon cart item list */
                Positioned(
                  right: AppGap.small,
                  top: AppBorderRadius.small,
                  child: GestureDetector(
                    onTap: () {},
                    child: Container(
                      width: 32,
                      height: 32,
                      decoration: const BoxDecoration(
                        color: AppColors.white,
                        shape: BoxShape.circle,
                      ),
                      child: const Icon(
                        Icons.add_shopping_cart,
                        size: AppFontSize.small,
                        color: AppColors.black,
                      ),
                    ),
                  ),
                )
              ],
            ),
            const Gap(height: AppGap.small),
            ColumnPadding(
              padding: const EdgeInsets.symmetric(horizontal: AppGap.small),
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /* title item list */
                Text(
                  title,
                  style: AppTextStyle.semiBold.copyWith(
                    color: AppColors.black,
                    fontSize: AppFontSize.normal,
                  ),
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
                const Gap(height: AppGap.small),

                /* button like */
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Row(
                      children: [
                        CustomInkwell(
                          onTap: () {},
                          child: const Icon(
                            Icons.thumb_up_alt_outlined,
                            size: AppIconSize.small,
                            color: AppColors.black,
                          ),
                        ),
                        const Gap(width: AppGap.tiny),
                        Text(
                          count,
                          style: AppTextStyle.semiBold.copyWith(
                            color: AppColors.black,
                            fontSize: AppFontSize.normal,
                          ),
                        ),
                      ],
                    ),
                    CustomInkwell(
                      onTap: () {},
                      child: const Icon(
                        Icons.share,
                        size: AppIconSize.small,
                        color: AppColors.black,
                      ),
                    )
                  ],
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
