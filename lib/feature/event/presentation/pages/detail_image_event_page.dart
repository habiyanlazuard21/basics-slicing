import 'package:flutter/material.dart';

import '../../../../lib.dart';

class DetailImageEventPage extends StatefulWidget {
  const DetailImageEventPage({
    Key? key,
    required DetailImageEventParams params,
  })  : _params = params,
        super(key: key);

  final DetailImageEventParams _params;

  @override
  State<DetailImageEventPage> createState() => _DetailImageEventPageState();
}

class _DetailImageEventPageState extends State<DetailImageEventPage> {
  /* List keyword */
  final List<String> _itemKeyword = DetailImageEventEntity.getItemKeyword;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            /* App Header */
            Padding(
              padding: const EdgeInsets.only(right: AppGap.normal),
              child: CustomAppBar(
                title: widget._params.title,
                elevation: 0,
                onPressedBack: () {
                  Navigator.pop(context);
                },
                onTapShare: () {},
              ),
            ),
            const Gap(height: AppGap.tiny),
            /* App Body */
            Expanded(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppGap.extraLarge),
                      child: CustomImageWrapper(
                        image: widget._params.image,
                        width: double.infinity,
                        height: 329,
                        fit: BoxFit.cover,
                        borderRadius: AppBorderRadius.small,
                        isNetworkImage: false,
                      ),
                    ),
                    const Gap(height: AppGap.normal),
                    RowPadding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppGap.extraLarge),
                      children: [
                        const CustomImageWrapper(
                          image: AppImages.basketball,
                          width: 48,
                          height: 48,
                          fit: BoxFit.cover,
                          borderRadius: AppBorderRadius.big,
                          isNetworkImage: false,
                        ),
                        const Gap(width: AppGap.small),
                        Flexible(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                widget._params.title,
                                style: AppTextStyle.semiBold.copyWith(
                                  color: AppColors.black,
                                  fontSize: AppFontSize.medium,
                                ),
                              ),
                              Text(
                                widget._params.subTitle,
                                style: AppTextStyle.medium.copyWith(
                                  color: AppColors.black,
                                  fontSize: AppFontSize.normal,
                                  overflow: TextOverflow.ellipsis,
                                ),
                                maxLines: 2,
                                softWrap: true,
                              ),
                            ],
                          ),
                        )
                      ],
                    ),
                    const Gap(height: AppGap.normal),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: AppGap.extraLarge,
                      ),
                      child: Text(
                        'Keyword',
                        style: AppTextStyle.semiBold.copyWith(
                          color: AppColors.black,
                          fontSize: AppFontSize.normal,
                        ),
                      ),
                    ),
                    const Gap(height: AppGap.small),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Container(
                        margin: const EdgeInsetsDirectional.symmetric(
                          horizontal: AppGap.extraLarge,
                        ),
                        child: Row(
                          children: List.generate(
                            _itemKeyword.length,
                            (index) {
                              return Padding(
                                padding: const EdgeInsets.only(
                                    right: AppGap.small,
                                    bottom: AppBorderRadius.normal),
                                child: ButtonPrimary(
                                  _itemKeyword[index],
                                  borderColor: AppColors.black,
                                  buttonColor: AppColors.white,
                                  elevation: 0,
                                  labelColor: AppColors.black,
                                  fontWeight: AppFontWeight.medium,
                                  fontSize: AppFontSize.small,
                                  height: 30,
                                  onPressed: () {},
                                ),
                              );
                            },
                          ),
                        ),
                      ),
                    ),
                    const Gap(height: AppGap.small),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: AppGap.extraLarge),
                      child: ButtonPrimary(
                        'Purchase',
                        width: double.infinity,
                        height: 48,
                        buttonColor: AppColors.black,
                        onPressed: () {},
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
