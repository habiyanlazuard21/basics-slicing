import 'package:flutter/material.dart';

class BottomNavigationEntity {
  final Widget icon;
  final String label;

  BottomNavigationEntity({
    required this.icon,
    required this.label,
  });

  static List<BottomNavigationEntity> bottomNavList = [
    BottomNavigationEntity(icon: const Icon(Icons.home), label: "Home"),
    BottomNavigationEntity(icon: const Icon(Icons.search), label: "Search"),
    BottomNavigationEntity(icon: const Icon(Icons.favorite), label: "Favorite"),
    BottomNavigationEntity(
        icon: const Icon(Icons.shopping_cart), label: "Shop"),
    BottomNavigationEntity(icon: const Icon(Icons.person), label: "Profile"),
  ];
}
