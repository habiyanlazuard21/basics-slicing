import 'package:flutter/material.dart';
import 'package:slicing_basics/feature/common/presentation/widgets/scaffold_constraint.dart';

import '../../../../lib.dart';

class BottomNavigation extends StatefulWidget {
  final int _selectedIndex;

  const BottomNavigation({
    Key? key,
    int selectedIndex = 0,
  })  : _selectedIndex = selectedIndex,
        super(key: key);

  @override
  State<BottomNavigation> createState() => _BottomNavigationState();
}

class _BottomNavigationState extends State<BottomNavigation>
    with TickerProviderStateMixin {
  /* Bottom Navigation */
  final List<BottomNavigationEntity> _bottomNavList =
      BottomNavigationEntity.bottomNavList;
  int _indexActive = 0;

  /* Scaffold Key*/
  final GlobalKey<ScaffoldState> _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return ScaffoldConstraint(
      keyScaffold: _key,
      onWillPop: () {},
      isBottomNavBar: true,
      bottomNavigationBar: ResponsiveUtils(context).getMediaQueryWidth() > 430
          ? null
          : _bottomNavigation(context),
      child: Column(
        children: [
          /* List Item */
          Expanded(
            child: IndexedStack(
              index: _indexActive,
              children: [
                HomeScreen(
                  params: HomeScreenParams(),
                ),
                const Center(
                  child: Text("SEARCH PAGE"),
                ),
                const Center(
                  child: Text("FAVORITE PAGE"),
                ),
                const Center(
                  child: Text("CART PAGE"),
                ),
                const Center(
                  child: Text("PROFILE PAGE"),
                ),
              ],
            ),
          ),
          if (ResponsiveUtils(context).getMediaQueryWidth() > 430) ...[
            _bottomNavigation(context),
          ],
        ],
      ),
    );
  }

  void _onItemSelected(int index) {
    setState(
      () {
        _indexActive = index;
      },
    );
  }

  BottomNavigationBar _bottomNavigation(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      items: <BottomNavigationBarItem>[
        ...List.generate(
          _bottomNavList.length,
          (index) => BottomNavigationBarItem(
            icon: _bottomNavList[index].icon,
            label: _bottomNavList[index].label,
          ),
        )
      ],
      currentIndex: _indexActive,
      selectedItemColor: AppColors.black,
      unselectedItemColor: AppColors.grey,
      showSelectedLabels: false,
      showUnselectedLabels: false,
      onTap: (index) => _onItemSelected(index),
    );
  }
}
