import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import './lib.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    final PageRouter router = PageRouter();
    return MultiBlocProvider(
      providers: [
        BlocProvider(
          create: (context) => PageCubit(),
        )
      ],
      child: MaterialApp(
        debugShowCheckedModeBanner: false,
        theme: FotogritTheme().of(context),
        onGenerateRoute: router.getRoute,
      ),
    );
  }
}
